@echo off
set "PROJECT=pruebas"
docker volume create "%PROJECT%_data"
title "Postgres %PROJECT%"
docker run -it --name %PROJECT%.postgres.db -e POSTGRES_PASSWORD=%PROJECT% -e POSTGRES_USER=%PROJECT% -e POSTGRES_DB=%PROJECT% -p 7001:5432 postgres:11.5
docker rm -fv %PROJECT%.postgres.db

