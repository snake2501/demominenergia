package com.demoprueba.repository;

import com.demoprueba.entity.Entidad;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author CMFerroV
 */
@Repository
public interface EntidadRepository extends JpaRepository<Entidad, Long> {

    @Query("select obj from Entidad obj")
    public Page<Entidad> findAll(Pageable pageable);

    @Query("select obj from Entidad obj where obj.id =:id")
    public Optional<Entidad> findById(@Param("id") Long id);

    @Query("select obj from Entidad obj where obj.nit =:nit")
    public Optional<Entidad> findByNit(@Param("nit") Integer nit);

    @Query("select obj from Entidad obj where obj.nombre =:nombre")
    public Optional<Entidad> findByNombre(@Param("nombre") String nombre);

}
