package com.demoprueba.repository;

import com.demoprueba.entity.Usuario;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author CMFerroV
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query("select obj from Usuario obj")
    public Page<Usuario> findAll(Pageable pageable);

    @Query("select obj from Usuario obj where obj.id =:id")
    public Optional<Usuario> findById(@Param("id") Long id);

    @Query("select obj from Usuario obj where obj.login =:login and obj.password =:password")
    public Optional<Usuario> findByLoginAndPassword(@Param("login") String login, @Param("password") String password);

    @Query("select obj from Usuario obj where obj.login =:login")
    public Optional<Usuario> findByLogin(@Param("login") String login);

    @Query("select obj from Usuario obj where obj.password =:password")
    public Optional<Usuario> findByPassword(@Param("password") String password);

}
