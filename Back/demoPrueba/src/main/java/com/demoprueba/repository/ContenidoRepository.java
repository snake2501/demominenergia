package com.demoprueba.repository;

import com.demoprueba.entity.Contenido;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author CMFerroV
 */
@Repository
public interface ContenidoRepository extends JpaRepository<Contenido, Long> {

    @Query("select obj from Contenido obj")
    public Page<Contenido> findAll(Pageable pageable);

    @Query("select obj from Contenido obj where obj.id =:id")
    public Optional<Contenido> findById(@Param("id") Long id);

    @Query("select obj from Contenido obj where obj.titulo =:titulo")
    public Optional<Contenido> findByTitulo(@Param("titulo") String titulo);

    @Query("select obj from Contenido obj where obj.descripcion =:descripcion")
    public Optional<Contenido> findByDescripcion(@Param("descripcion") String descripcion);

    @Query("select obj from Contenido obj where obj.fecha =:fecha")
    public Optional<Contenido> findByFecha(@Param("fecha") Date fecha);

}
