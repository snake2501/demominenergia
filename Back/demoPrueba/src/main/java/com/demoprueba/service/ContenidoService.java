package com.demoprueba.service;

import com.demoprueba.repository.ContenidoRepository;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.demoprueba.entity.*;

/**
 *
 * @author CMFerroV
 */
@Service
public class ContenidoService {

    @Autowired
    private ContenidoRepository contenidoRepository;

    public Contenido saveOrUpdateContenido(Contenido contenido) {
        return contenidoRepository.save(contenido);
    }

    public Boolean deleteContenido(Contenido contenido) {
        try {
            contenidoRepository.delete(contenido);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Page<Contenido> allContenido(Pageable pageable) {
        return contenidoRepository.findAll(pageable);
    }

    public Optional<Contenido> contenidoById(Long id) {
        return contenidoRepository.findById(id);
    }

    public Optional<Contenido> contenidoByTitulo(String titulo) {
        return contenidoRepository.findByTitulo(titulo);
    }

    public Optional<Contenido> contenidoByDescripcion(String descripcion) {
        return contenidoRepository.findByDescripcion(descripcion);
    }

    public Optional<Contenido> contenidoByFecha(Date fecha) {
        return contenidoRepository.findByFecha(fecha);
    }

}
