package com.demoprueba.service;

import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.demoprueba.entity.*;
import com.demoprueba.repository.EntidadRepository;

/**
 *
 * @author CMFerroV
 */
@Service
public class EntidadService {

    @Autowired
    private EntidadRepository entidadRepository;

    public Entidad saveOrUpdateEntidad(Entidad entidad) {
        return entidadRepository.save(entidad);
    }

    public Boolean deleteEntidad(Entidad entidad) {
        try {
            entidadRepository.delete(entidad);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Page<Entidad> allEntidad(Pageable pageable) {
        return entidadRepository.findAll(pageable);
    }

    public Optional<Entidad> entidadById(Long id) {
        return entidadRepository.findById(id);
    }

    public Optional<Entidad> entidadByNit(Integer nit) {
        return entidadRepository.findByNit(nit);
    }

    public Optional<Entidad> entidadByNombre(String nombre) {
        return entidadRepository.findByNombre(nombre);
    }

}
