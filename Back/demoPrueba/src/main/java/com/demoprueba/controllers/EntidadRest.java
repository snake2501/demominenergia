package com.demoprueba.controllers;

import com.demoprueba.service.EntidadService;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import com.demoprueba.dtos.*;
import com.demoprueba.entity.*;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Optional;
import static org.hibernate.internal.util.StringHelper.isBlank;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author CMFerroV
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController("entidad")
public class EntidadRest {

    @Autowired
    private EntidadService entidadService;

    @Autowired
    private ModelMapper modelMapper;

    private EntidadDTO convertToDto(Entidad post) {
        EntidadDTO postDto = modelMapper.map(post, EntidadDTO.class);
        return postDto;
    }

    private EntidadDTO convertToDto(Optional<Entidad> postOpt) {
        Entidad post = null;
        if (postOpt.isPresent()) {
            post = postOpt.get();
        } else {
            return null;
        }
        return convertToDto(post);
    }

    private Page<EntidadDTO> convertToDto(Page<Entidad> post, Pageable pageable) {
        List<EntidadDTO> postDto = new ArrayList<>();

        for (Entidad entity : post.getContent()) {
            postDto.add(convertToDto(entity));
        }

        return new PageImpl<>(postDto, pageable, post.getTotalElements());
    }

    private Entidad convertToEntity(EntidadDTO postDto) {
        Entidad post = modelMapper.map(postDto, Entidad.class);
        return post;
    }

    @ApiOperation(value = "")
    @PostMapping(path = "entidad/saveorupdate")
    public EntidadDTO saveOrUpdateEntidads(@RequestBody EntidadDTO Entidads) {
        return convertToDto(entidadService.saveOrUpdateEntidad(convertToEntity(Entidads)));
    }

    @ApiOperation(value = "")
    @PostMapping(path = "entidad/delete")
    public Boolean deleteEntidads(@RequestBody EntidadDTO Entidads) {
        try {
            entidadService.deleteEntidad(convertToEntity(Entidads));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "")
    @GetMapping("entidad/all")
    public Page<EntidadDTO> allEntidad(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer pagina,
            @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(defaultValue = "1") int sortOrderBy,
            @RequestParam(name = "filtro", required = false) String filtro) {
        Sort sort = isBlank(sortBy) ? Sort.unsorted() : Sort.by(sortOrderBy == 1 ? Sort.Direction.ASC : Sort.Direction.DESC, sortBy);
        Pageable pageable = PageRequest.of(pagina, size, sort);
        return convertToDto(entidadService.allEntidad(pageable), pageable);
    }

    @ApiOperation(value = "")
    @GetMapping("entidad/ById/{id}")
    public EntidadDTO entidadsById(@PathVariable Long id) {
        return convertToDto(entidadService.entidadById(id).get());
    }

    @ApiOperation(value = "")
    @GetMapping("entidad/ByNit/{nit}")
    public EntidadDTO entidadsByNit(@PathVariable Integer nit) {
        return convertToDto(entidadService.entidadByNit(nit).get());
    }

    @ApiOperation(value = "")
    @GetMapping("entidad/ByNombre/{nombre}")
    public EntidadDTO entidadsByNombre(@PathVariable String nombre) {
        return convertToDto(entidadService.entidadByNombre(nombre).get());
    }

}
