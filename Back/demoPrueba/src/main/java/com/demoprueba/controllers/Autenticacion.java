package com.demoprueba.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import com.demoprueba.dtos.UserLogin;
import com.demoprueba.entity.Usuario;
import com.demoprueba.service.UsuarioService;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author CMFerroV
 */
@RestController
@RequestMapping("Autenticacion")
@CrossOrigin
public class Autenticacion {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
    public UserLogin login(@RequestBody UserLogin userRequest) {

        Usuario usuario = null;
        Optional<Usuario> result = usuarioService.usuarioByLoginPassword(userRequest.login, userRequest.Password);
        if (result.isPresent()) {
            usuario = result.get();
            UserLogin user = new UserLogin();
            user.setId(usuario.getId());
            user.setNombre(usuario.getNombres() + usuario.getApellidos());
            user.setLogin(usuario.getLogin());
            return user;
        }
        return null;
    }

}
