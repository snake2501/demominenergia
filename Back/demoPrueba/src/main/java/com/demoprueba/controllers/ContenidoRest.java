package com.demoprueba.controllers;

import java.util.List;
import com.demoprueba.entity.*;
import com.demoprueba.dtos.*;
import com.demoprueba.service.ContenidoService;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import static org.hibernate.internal.util.StringHelper.isBlank;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author CMFerroV
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController("contenido")
public class ContenidoRest {

    @Autowired
    private ContenidoService contenidoService;

    @Autowired
    private ModelMapper modelMapper;

    private ContenidoDTO convertToDto(Contenido post) {
        ContenidoDTO postDto = modelMapper.map(post, ContenidoDTO.class);
        return postDto;
    }

    private ContenidoDTO convertToDto(Optional<Contenido> postOpt) {
        Contenido post = null;
        if (postOpt.isPresent()) {
            post = postOpt.get();
        } else {
            return null;
        }
        return convertToDto(post);
    }

    private Page<ContenidoDTO> convertToDto(Page<Contenido> post, Pageable pageable) {
        List<ContenidoDTO> postDto = new ArrayList<>();

        for (Contenido entity : post.getContent()) {
            postDto.add(convertToDto(entity));
        }

        return new PageImpl<>(postDto, pageable, post.getTotalElements());
    }

    private Contenido convertToEntity(ContenidoDTO postDto) {
        Contenido post = modelMapper.map(postDto, Contenido.class);
        return post;
    }

    @ApiOperation(value = "")
    @PostMapping(path = "contenido/saveorupdate")
    public ContenidoDTO saveOrUpdateContenidos(@RequestBody ContenidoDTO Contenidos) {
        return convertToDto(contenidoService.saveOrUpdateContenido(convertToEntity(Contenidos)));
    }

    @ApiOperation(value = "")
    @PostMapping(path = "contenido/delete")
    public Boolean deleteContenidos(@RequestBody ContenidoDTO Contenidos) {
        try {
            contenidoService.deleteContenido(convertToEntity(Contenidos));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "")
    @GetMapping("contenido/all")
    public Page<ContenidoDTO> allContenido(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer pagina,
            @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(defaultValue = "1") int sortOrderBy,
            @RequestParam(name = "filtro", required = false) String filtro) {
        Sort sort = isBlank(sortBy) ? Sort.unsorted() : Sort.by(sortOrderBy == 1 ? Sort.Direction.ASC : Sort.Direction.DESC, sortBy);
        Pageable pageable = PageRequest.of(pagina, size, sort);
        return convertToDto(contenidoService.allContenido(pageable), pageable);
    }

    @ApiOperation(value = "")
    @GetMapping("contenido/ById/{id}")
    public ContenidoDTO contenidosById(@PathVariable Long id) {
        return convertToDto(contenidoService.contenidoById(id).get());
    }

    @ApiOperation(value = "")
    @GetMapping("contenido/ByTitulo/{titulo}")
    public ContenidoDTO contenidosByTitulo(@PathVariable String titulo) {
        return convertToDto(contenidoService.contenidoByTitulo(titulo).get());
    }

    @ApiOperation(value = "")
    @GetMapping("contenido/ByDescripcion/{descripcion}")
    public ContenidoDTO contenidosByDescripcion(@PathVariable String descripcion) {
        return convertToDto(contenidoService.contenidoByDescripcion(descripcion).get());
    }

    @ApiOperation(value = "")
    @GetMapping("contenido/ByFecha/{fecha}")
    public ContenidoDTO contenidosByFecha(@PathVariable Date fecha) {
        return convertToDto(contenidoService.contenidoByFecha(fecha).get());
    }

}
