package com.demoprueba.controllers;

import com.demoprueba.dtos.UsuarioDTO;
import com.demoprueba.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiOperation;
import static org.hibernate.internal.util.StringHelper.isBlank;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author CMFerroV
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController("usuario")
public class UsuarioRest {

    @Autowired
    private UsuarioService usuarioService;

    @ApiOperation(value = "")
    @PostMapping(path = "usuario/saveorupdate")
    public UsuarioDTO saveOrUpdateUsuarios(@RequestBody UsuarioDTO Usuarios) {
        return usuarioService.convertToDto(usuarioService.saveOrUpdateUsuario(usuarioService.convertToEntity(Usuarios)));
    }

    @ApiOperation(value = "")
    @PostMapping(path = "usuario/delete")
    public Boolean deleteUsuarios(@RequestBody UsuarioDTO Usuarios) {
        try {
            usuarioService.deleteUsuario(usuarioService.convertToEntity(Usuarios));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "")
    @GetMapping("usuario/all")
    public Page<UsuarioDTO> allUsuario(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer pagina,
            @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(defaultValue = "1") int sortOrderBy,
            @RequestParam(name = "filtro", required = false) String filtro) {
        Sort sort = isBlank(sortBy) ? Sort.unsorted() : Sort.by(sortOrderBy == 1 ? Sort.Direction.ASC : Sort.Direction.DESC, sortBy);
        Pageable pageable = PageRequest.of(pagina, size, sort);
        return usuarioService.convertToDto(usuarioService.allUsuario(pageable), pageable);
    }

    @ApiOperation(value = "")
    @GetMapping("usuario/ById/{id}")
    public UsuarioDTO usuariosById(@PathVariable Long id) {
        return usuarioService.convertToDto(usuarioService.usuarioById(id).get());
    }

    @ApiOperation(value = "")
    @GetMapping("usuario/Autenticacion/{login}/{passwrod}")
    public UsuarioDTO usuariosByLogin(@PathVariable String login,@PathVariable String password) {
        return usuarioService.convertToDto(usuarioService.usuarioByLogin(login).get());
    }
    
    @ApiOperation(value = "")
    @GetMapping("usuario/ByLogin/{login}")
    public UsuarioDTO usuariosByLogin(@PathVariable String login) {
        return usuarioService.convertToDto(usuarioService.usuarioByLogin(login).get());
    }

    @ApiOperation(value = "")
    @GetMapping("usuario/ByPassword/{password}")
    public UsuarioDTO usuariosByPassword(@PathVariable String password) {
        return usuarioService.convertToDto(usuarioService.usuarioByPassword(password).get());
    }

}
