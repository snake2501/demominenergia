package com.demoprueba.dtos;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Carlos Ferro
 */
@Data
public class ContenidoDTO implements Serializable {

    private Long id;
    private String titulo;
    private String descripcion;
    private Date fecha;
    private Long id_usuario;


}

