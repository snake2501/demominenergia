package com.demoprueba.dtos;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Carlos Ferro
 */
@Data
public class EntidadDTO implements Serializable {

    private Long id;
    private Integer nit;
    private String nombre;


}

