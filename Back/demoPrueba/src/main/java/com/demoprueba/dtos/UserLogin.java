/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demoprueba.dtos;

import lombok.Data;

/**
 *
 * @author Carlos Mario Ferro
 */
@Data
public class UserLogin {

    public Long Id;
    public String Nombre;
    public String Password;
    public String Rol;
    public String login;

}
