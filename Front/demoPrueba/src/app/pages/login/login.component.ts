import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;

  constructor(private router: Router, public _aut: AutenticacionService, private fb: FormBuilder) { }

  ngOnInit() {
    if (this._aut.autenticado) {
      this.router.navigate(["/UsuarioList"]);
    }

    this.form = this.fb.group({
      login: ["", [Validators.required]],
      password: ["", [Validators.required, Validators.minLength(5)]]
    });
  }


  ingresar() {
    this._aut.iniciarSesion(this.form.value).subscribe((res: Response) => {
      console.log("aut",res)
      if(res){
        this._aut.inicializarSesionUSuario(<any>res);
        this.router.navigate(["/UsuarioList"]);
      }
    });
  }

}

