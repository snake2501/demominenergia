import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LazyLoadEvent, SelectItem, MessageService } from 'primeng/api';


import { EncuestaDTO } from './encuesta.dto';
import { Page } from 'src/app/common/util';
import { HttpParams, HttpResponse } from '@angular/common/http';
import { encuestaService } from 'src/app/core/services/encuesta.service';
import { marcaPcService } from 'src/app/core/services/marcaPc.service';
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';
import { marcafavorita } from 'src/app/core/interfaces/marcafavorita.interface';
import { Router } from '@angular/router';
import { calendar_es } from 'src/environments/environment';


@Component({
  selector: 'app-encuesta-List',
  templateUrl: './encuesta-List.component.html',
  providers: [MessageService]
})
export class EncuestaListComponent implements OnInit {

  form: FormGroup;

  nav: any = {};


  id: number;
  id_usuario: number;
  ndocumento: number;
  email: string;
  comentario: string;
  marcafavorita: marcafavorita;
  marcafavoritas: marcafavorita[];
  fecha: Date;
  filtro: string;

  encuestas: EncuestaDTO[];
  encuestaPage: Page<EncuestaDTO>;
  totalRecords: number;
  cols: any[];
  loading: boolean;
  pagitatorTable: boolean;
  rowsTable: number;
  sortBy: string;
  sortOrderBy: string;

  displayDialog: boolean;
  encuesta: EncuestaDTO = {};
  selectedEncuesta: EncuestaDTO;
  newencuesta: boolean;
  eventoTabla: LazyLoadEvent;

  es: any;

  constructor(private fb: FormBuilder, private _marcaPcService: marcaPcService, private _encuestaService: encuestaService,
    public _aut: AutenticacionService, private router: Router) {

    this.nav = { back: '', next: '' };

    this.marcafavoritas = [
      { nombre: 'Seleccione', codigo: '' },];

    this._marcaPcService.all().subscribe((res: HttpResponse<Object>) => {
      let result: any = res;
      result.forEach(res => {
        console.log("res", res.nombre, res.id)
        this.marcafavoritas.push({ nombre: res.nombre, codigo: res.id });
      });
    });

    this.cols = [
      { field: 'id', header: 'id' },
      { field: 'id_usuario', header: 'id_usuario' },
      { field: 'ndocumento', header: 'ndocumento' },
      { field: 'email', header: 'email' },
      { field: 'comentario', header: 'comentario' },
      { field: 'marcafavorita', header: 'marcafavorita' },
      { field: 'fecha', header: 'fecha' },

    ];

  }

  ngOnInit() {
    this.loading = true;
    this.pagitatorTable = true;
    this.rowsTable = 10;
    this.es = calendar_es;

    this.form = this.fb.group({
      id: new FormControl(''),
      id_usuario: new FormControl(''),
      ndocumento: new FormControl(''),
      email: new FormControl(''),
      comentario: new FormControl(''),
      marcafavorita: new FormControl(''),
      fecha: new FormControl(new Date())
    });

    this.id = null;
    this.id_usuario = null;
    this.ndocumento = null;
    this.email = null;
    this.comentario = null;
    this.marcafavorita = null;
    this.fecha = null;
  }

  resetPage() {
    this.ngOnInit();
    this.loadEncuestaLazy(this.eventoTabla);
  }

  filtrar() {
    this.loadEncuestaLazy(this.eventoTabla);
  }

  onSubmit(value: string) {
    console.log("submit ok", this.form);
  }

  loadEncuestaLazy(event: LazyLoadEvent) {
    this.loading = true;
    this.eventoTabla = event;
    this.sortBy = event.sortField;
    this.sortOrderBy = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.filtro = this.filtro === '' ? undefined : this.filtro;

    this._encuestaService.all(event.first / event.rows, event.rows, this.filtro, this.sortBy, this.sortOrderBy).subscribe((res: HttpResponse<Object>) => {
      let page: any = res;
      this.encuestaPage = page.body;
      let respuesta = this.encuestaPage.content;
      this.totalRecords = this.encuestaPage.totalElements;
      this.encuestas = respuesta;
      console.log("this.encuestas", this.encuestas)
      this.loading = false;
    });

  }





  showDialogToAdd() {
    this.newencuesta = true;
    this.encuesta = {};
    this.displayDialog = true;
  }

  castObjectInterface() {
    this.selectedEncuesta = {};

    this.selectedEncuesta.id = this.id;
    this.selectedEncuesta.id_usuario = this.id_usuario;
    this.selectedEncuesta.ndocumento = this.ndocumento;
    this.selectedEncuesta.email = this.email;
    this.selectedEncuesta.comentario = this.comentario;
    this.selectedEncuesta.marcafavorita = this.marcafavorita.codigo;
    this.selectedEncuesta.fecha = this.fecha;
  }

  save() {
    this.loading = true;
    this.castObjectInterface();
    this._encuestaService.addEncuesta(this.selectedEncuesta).subscribe((result) => {
      this.resetPage();
      this.loading = false;
    }, (err) => {
      console.log(err);
      this.loading = false;
    });
  }

  clean() {
    this.resetPage();
  }

  delete(id) {
    this.displayDialog = false;
    this._encuestaService.deleteEncuesta(id).subscribe((res) => {
      this.loadEncuestaLazy(this.eventoTabla);
    })
  }

  onRowSelect(event) {
    //this.loadSelect(event.data.id);
  }

  loadSelect(id) {
    this._encuestaService.obtenerEncuestaPorid(id).subscribe((res: HttpResponse<Object>) => {
      let respuesta: any = res;
      console.log("byId");
      console.log(respuesta);
      if (respuesta != null) {

        this.id = respuesta.id;
        this.id_usuario = respuesta.id_usuario;
        this.ndocumento = respuesta.ndocumento;
        this.email = respuesta.email;
        this.comentario = respuesta.comentario;
        this.marcafavorita = this.marcafavoritas.find(marcafavorita => Number(marcafavorita.codigo) === Number(respuesta.marcafavorita));
        console.log("this.marcafavorita",this.marcafavoritas)
        console.log("this.marcafavorita",this.marcafavorita)
        this.fecha = new Date(respuesta.fecha);
      }
      this.loading = false;
    });
  }

  cloneencuesta(c: EncuestaDTO): EncuestaDTO {
    let encuesta = {};
    for (let prop in c) {
      encuesta[prop] = c[prop];
    }
    return encuesta;
  }



}








