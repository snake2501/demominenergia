import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LazyLoadEvent, SelectItem, MessageService } from 'primeng/api';

import { UsuarioDTO } from '../usuario/usuario.dto';
import { ContenidoDTO } from './contenido.dto';
import { Page } from 'src/app/common/util';
import { HttpParams, HttpResponse } from '@angular/common/http';
import { contenidoService } from 'src/app/core/services/contenido.service';
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';
import { usuarioService } from 'src/app/core/services/usuario.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-contenido-List',
  templateUrl: './contenido-List.component.html',
  providers: [MessageService]
})
export class ContenidoListComponent implements OnInit {

  form: FormGroup;

  nav: any = {};


  id: number;
  titulo: string;
  descripcion: string;
  fecha: Date;
  id_usuario:UsuarioDTO;
  filtro: string;

  contenidos: ContenidoDTO[];
  contenidoPage: Page<ContenidoDTO>;
  totalRecords: number;
  cols: any[];
  loading: boolean;
  pagitatorTable: boolean;
  rowsTable: number;
  sortBy: string;
  sortOrderBy: string;

  displayDialog: boolean;
  contenido: ContenidoDTO = {};
  selectedContenido: ContenidoDTO;
  newcontenido: boolean;
  eventoTabla: LazyLoadEvent;

  usuarios: UsuarioDTO[];

  constructor(private fb: FormBuilder, private messageService: MessageService, private _contenidoService: contenidoService,private _usuarioService: usuarioService,
    public _aut: AutenticacionService, private router: Router) {

    this.nav = { back: '', next: '' };

    this.usuarios = [
      { login: 'Seleccione', id: '' },];

    this._usuarioService.all(0, 100, "").subscribe((res: HttpResponse<Object>) => {
      console.log("res.body", res.body)
      let result: any = res.body;
      result.content.forEach(res => {
        console.log("res", res.login, res.id)
        this.usuarios.push({ login: res.login, id: res.id });
      });
    });


    this.cols = [
      { field: 'id', header: 'id' },
      { field: 'titulo', header: 'titulo' },
      { field: 'descripcion', header: 'descripcion' },
      { field: 'fecha', header: 'fecha' },
      { field: 'id_usuario', header: 'Usuario' },
      

    ];

  }

  ngOnInit() {
    this.loading = true;
    this.pagitatorTable = true;
    this.rowsTable = 10;

    this.form = this.fb.group({
      id: new FormControl(''),
      titulo: new FormControl(''),
      descripcion: new FormControl(''),
      fecha: new FormControl(''),
      id_usuario: new FormControl(''),
    });

    this.id = null;
    this.titulo = null;
    this.descripcion = null;
    this.fecha = null;
    this.id_usuario = null;
  }

  resetPage() {
    this.ngOnInit();
    this.loadContenidoLazy(this.eventoTabla);
  }

  filtrar() {
    this.loadContenidoLazy(this.eventoTabla);
  }

  onSubmit(value: string) {
    console.log("submit ok", this.form);
  }

  loadContenidoLazy(event: LazyLoadEvent) {
    this.loading = true;
    this.eventoTabla = event;
    this.sortBy = event.sortField;
    this.sortOrderBy = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.filtro = this.filtro === '' ? undefined : this.filtro;

    this._contenidoService.all(event.first / event.rows, event.rows, this.filtro, this.sortBy, this.sortOrderBy).subscribe((res: HttpResponse<Object>) => {
      let page: any = res;
      this.contenidoPage = page.body;
      let respuesta = this.contenidoPage.content;
      this.totalRecords = this.contenidoPage.totalElements;
      this.contenidos = respuesta;
      this.loading = false;
    });

  }

  showDialogToAdd() {
    this.newcontenido = true;
    this.contenido = {};
    this.displayDialog = true;
  }

  castObjectInterface() {
    this.selectedContenido = {};

    this.selectedContenido.id = this.id;
    this.selectedContenido.titulo = this.titulo;
    this.selectedContenido.descripcion = this.descripcion;
    this.selectedContenido.fecha = new Date();
    this.selectedContenido.id_usuario = this.id_usuario.id;
    
  }

  save() {
    this.loading = true;
    this.castObjectInterface();
    this._contenidoService.addContenido(this.selectedContenido).subscribe((result) => {
      this.resetPage();
      this.loading = false;
    }, (err) => {
      console.log(err);
      this.loading = false;
    });
  }

  clean() {
    this.resetPage();
  }

  delete() {
    let index = this.contenidos.indexOf(this.selectedContenido);
    this.contenidos = this.contenidos.filter((val, i) => i != index);
    this.contenido = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    //this.loadSelect(event.data.id);
  }

  loadSelect(id) {
    this._contenidoService.obtenerContenidoPorid(id).subscribe((res: HttpResponse<Object>) => {
      let respuesta: any = res;
      console.log("byId");
      console.log(respuesta);
      if (respuesta != null) {

        this.id = respuesta.id;
        this.titulo = respuesta.titulo;
        this.descripcion = respuesta.descripcion;
        //this.fecha = respuesta.fecha;
        this.id_usuario = this.usuarios.find(usuario => Number(usuario.id) === Number(respuesta.id_usuario));
      }
      this.loading = false;
    });
  }

  clonecontenido(c: ContenidoDTO): ContenidoDTO {
    let contenido = {};
    for (let prop in c) {
      contenido[prop] = c[prop];
    }
    return contenido;
  }


}





