import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LazyLoadEvent, SelectItem, MessageService } from 'primeng/api';

import { EntidadDTO } from './entidad.dto';
import { Page } from 'src/app/common/util';
import { HttpParams, HttpResponse } from '@angular/common/http';
import { entidadService } from 'src/app/core/services/entidad.service';
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-entidad-List',
  templateUrl: './entidad-List.component.html',
  providers: [MessageService]
})
export class EntidadListComponent implements OnInit {

  form: FormGroup;

  nav: any = {};


  id: number;
  nit: number;
  nombre: string;
  filtro: string;

  entidads: EntidadDTO[];
  entidadPage: Page<EntidadDTO>;
  totalRecords: number;
  cols: any[];
  loading: boolean;
  pagitatorTable: boolean;
  rowsTable: number;
  sortBy: string;
  sortOrderBy: string;

  displayDialog: boolean;
  entidad: EntidadDTO = {};
  selectedEntidad: EntidadDTO;
  newentidad: boolean;
  eventoTabla: LazyLoadEvent;

  constructor(private fb: FormBuilder, private messageService: MessageService, private _entidadService: entidadService,
    public _aut: AutenticacionService, private router: Router) {

    this.nav = { back: '', next: '' };



    this.cols = [
      { field: 'id', header: 'id' },
      { field: 'nit', header: 'nit' },
      { field: 'nombre', header: 'nombre' },

    ];

  }

  ngOnInit() {
    this.loading = true;
    this.pagitatorTable = true;
    this.rowsTable = 10;

    this.form = this.fb.group({
      id: new FormControl(''),
      nit: new FormControl(''),
      nombre: new FormControl('')
    });

    this.id = null;
    this.nit = null;
    this.nombre = null;
  }

  resetPage() {
    this.ngOnInit();
    this.loadEntidadLazy(this.eventoTabla);
  }

  filtrar() {
    this.loadEntidadLazy(this.eventoTabla);
  }

  onSubmit(value: string) {
    console.log("submit ok", this.form);
  }

  loadEntidadLazy(event: LazyLoadEvent) {
    this.loading = true;
    this.eventoTabla = event;
    this.sortBy = event.sortField;
    this.sortOrderBy = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.filtro = this.filtro === '' ? undefined : this.filtro;

    this._entidadService.all(event.first / event.rows, event.rows, this.filtro, this.sortBy, this.sortOrderBy).subscribe((res: HttpResponse<Object>) => {
      let page: any = res;
      this.entidadPage = page.body;
      let respuesta = this.entidadPage.content;
      this.totalRecords = this.entidadPage.totalElements;
      this.entidads = respuesta;
      this.loading = false;
    });

  }

  showDialogToAdd() {
    this.newentidad = true;
    this.entidad = {};
    this.displayDialog = true;
  }

  castObjectInterface() {
    this.selectedEntidad = {};

    this.selectedEntidad.id = this.id;
    this.selectedEntidad.nit = this.nit;
    this.selectedEntidad.nombre = this.nombre;
  }

  save() {
    this.loading = true;
    this.castObjectInterface();
    this._entidadService.addEntidad(this.selectedEntidad).subscribe((result) => {
      this.resetPage();
      this.loading = false;
    }, (err) => {
      console.log(err);
      this.loading = false;
    });
  }

  clean() {
    this.resetPage();
  }

  delete() {
    let index = this.entidads.indexOf(this.selectedEntidad);
    this.entidads = this.entidads.filter((val, i) => i != index);
    this.entidad = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    //this.loadSelect(event.data.id);
  }

  loadSelect(id) {
    this._entidadService.obtenerEntidadPorid(id).subscribe((res: HttpResponse<Object>) => {
      let respuesta: any = res;
      console.log("byId");
      console.log(respuesta);
      if (respuesta != null) {

        this.id = respuesta.id;
        this.nit = respuesta.nit;
        this.nombre = respuesta.nombre;
      }
      this.loading = false;
    });
  }

  cloneentidad(c: EntidadDTO): EntidadDTO {
    let entidad = {};
    for (let prop in c) {
      entidad[prop] = c[prop];
    }
    return entidad;
  }


}




