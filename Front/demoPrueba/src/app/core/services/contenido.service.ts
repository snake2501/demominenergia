import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Page } from '../../common/util';
import { ContenidoDTO} from '../../pages/contenido/contenido.dto';
import { dominio } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class contenidoService {

  constructor(private http: HttpClient) { }

   all(page, pageSize, nameLike, sortBy, sortOrder): Observable<Object> {
    console.log('all contenido');
	let sort = "";
    if (sortBy !== undefined) {
      sort = "&sortBy=" + page ;
    }
	return this.http.get<HttpResponse<Object>>(dominio +  "contenido/all?filtro=" + nameLike + "&size=" + pageSize + "&page=" + page , {
    observe: 'response'
  });
  }  
  
  addContenido(contenido): Observable<ContenidoDTO> {
    console.log(contenido);
    return this.http.post<HttpResponse<Object>>(dominio +  'contenido/saveorupdate', contenido).pipe(
      tap((contenido) => console.log(`added contenido w/ id=${contenido}`)),
      catchError(this.handleError<any>('addcontenido'))
    );
  }

  updateContenido(id, contenido): Observable<ContenidoDTO> {
    return this.http.post<HttpResponse<Object>>(dominio +  'contenido/saveorupdate/' + id, contenido).pipe(
      tap(_ => console.log(`updated contenido id=${id}`)),
      catchError(this.handleError<any>('updatecontenido'))
    );
  }

  deleteContenido(id): Observable<ContenidoDTO> {
    return this.http.post<HttpResponse<Object>>(dominio +  'contenido/delete/' , id).pipe(
      tap(_ => console.log(`deleted contenido id=${id}`)),
      catchError(this.handleError<any>('deletecontenido'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  

  obtenerContenidoPorid(id: string): Observable<HttpResponse<ContenidoDTO>> {
    console.log('Obtener contenidoPorid');
    return this.http.get<HttpResponse<Object>>(dominio +  "contenido/ById/"+id);
  }

  obtenerContenidoPortitulo(titulo: string): Observable<HttpResponse<ContenidoDTO>> {
    console.log('Obtener contenidoPortitulo');
    return this.http.get<HttpResponse<Object>>(dominio +  "contenido/ByTitulo/"+titulo);
  }

  obtenerContenidoPordescripcion(descripcion: string): Observable<HttpResponse<ContenidoDTO>> {
    console.log('Obtener contenidoPordescripcion');
    return this.http.get<HttpResponse<Object>>(dominio +  "contenido/ByDescripcion/"+descripcion);
  }

  obtenerContenidoPorfecha(fecha: string): Observable<HttpResponse<ContenidoDTO>> {
    console.log('Obtener contenidoPorfecha');
    return this.http.get<HttpResponse<Object>>(dominio +  "contenido/ByFecha/"+fecha);
  }




}

