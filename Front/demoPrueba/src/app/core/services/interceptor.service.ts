import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { Observable, of } from 'rxjs';
import { catchError, map } from "rxjs/operators";
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {


  constructor(private _mess: MessageService, private _util: UtilService) { }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


    const reqClone = req.clone({ setHeaders: { 'Content-Type': 'application/json' } });


    return next.handle(reqClone).pipe(
      map(event => {
        this._util.cargando = true;
        if (event instanceof HttpResponse) {
          this._util.cargando = false;
          return event;
        }
      }),
      catchError(error => {
        this._util.cargando = false;
        if (error.status == 500) {
          if(error.error.errorSMC != null){ 
            this._mess.add({ severity: 'warn', summary: 'Alerta', detail: error.error.errorSMC });
          } else {
            this._mess.add({ severity: 'error', summary: 'Error: consumiendo el servicio '+error.error.path+', fecha: '+error.error.timestamp , detail: error.error.message });
          }
        } else {
          this._mess.add({ severity: 'error', summary: 'Error', detail: error.message });
        }
        return of(error);
      })
    );


  }


}

