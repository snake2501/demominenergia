import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Page } from '../../common/util';
import { UsuarioDTO } from '../../pages/usuario/usuario.dto';
import { dominio } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class usuarioService {

  constructor(private http: HttpClient) { }

  all(page, pageSize, nameLike): Observable<Object> {
    console.log('all usuario');
    
    return this.http.get<HttpResponse<Object>>(dominio + "usuario/all?filtro=" + nameLike + "&size=" + pageSize + "&page=" + page, {
      observe: 'response'
    });
  }

  addUsuario(usuario): Observable<UsuarioDTO> {
    console.log("usuario=",usuario);
    return this.http.post<HttpResponse<Object>>(dominio + 'usuario/saveorupdate', usuario).pipe(
      tap((usuario) => console.log(`added usuario w/ id=${usuario}`)),
      catchError(this.handleError<any>('addusuario'))
    );
  }

  updateUsuario(id, usuario): Observable<UsuarioDTO> {
    return this.http.post<HttpResponse<Object>>(dominio + 'usuario/saveorupdate/' + id, usuario).pipe(
      tap(_ => console.log(`updated usuario id=${id}`)),
      catchError(this.handleError<any>('updateusuario'))
    );
  }

  deleteUsuario(id): Observable<UsuarioDTO> {
    return this.http.post<HttpResponse<Object>>(dominio + 'usuario/delete/', id).pipe(
      tap(_ => console.log(`deleted usuario id=${id}`)),
      catchError(this.handleError<any>('deleteusuario'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  obtenerUsuarioPorid(id: string): Observable<HttpResponse<UsuarioDTO>> {
    console.log('Obtener usuarioPorid');
    return this.http.get<HttpResponse<Object>>(dominio +  "usuario/ById/"+id);
  }

  obtenerUsuarioPorlogin(login: string): Observable<HttpResponse<UsuarioDTO>> {
    console.log('Obtener usuarioPorlogin');
    return this.http.get<HttpResponse<Object>>(dominio +  "usuario/ByLogin/"+login);
  }

  obtenerUsuarioPorpassword(password: string): Observable<HttpResponse<UsuarioDTO>> {
    console.log('Obtener usuarioPorpassword');
    return this.http.get<HttpResponse<Object>>(dominio +  "usuario/ByPassword/"+password);
  }

  obtenerUsuarioPorintentos(intentos: string): Observable<HttpResponse<UsuarioDTO>> {
    console.log('Obtener usuarioPorintentos');
    return this.http.get<HttpResponse<Object>>(dominio +  "usuario/ByIntentos/"+intentos);
  }

  obtenerUsuarioPorultimoingreso(ultimoingreso: string): Observable<HttpResponse<UsuarioDTO>> {
    console.log('Obtener usuarioPorultimoingreso');
    return this.http.get<HttpResponse<Object>>(dominio +  "usuario/ByUltimoingreso/"+ultimoingreso);
  }




}

