import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Page } from '../../common/util';
import { UsuarioDTO } from '../../pages/usuario/usuario.dto';
import { dominio } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class marcaPcService {

  constructor(private http: HttpClient) { }

  all(): Observable<HttpResponse<UsuarioDTO>> {
    console.log('Obtener marca pc all');
    return this.http.get<HttpResponse<Object>>(dominio +  "marcaPc/all");
  }

}

