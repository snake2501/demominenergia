import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Page } from '../../common/util';
import { EntidadDTO } from '../../pages/entidad/entidad.dto';
import { dominio } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class entidadService {

  constructor(private http: HttpClient) { }

  all(page, pageSize, nameLike): Observable<Object> {
    console.log('all entidad');
    return this.http.get<HttpResponse<Object>>(dominio + "entidad/all?filtro=" + nameLike + "&size=" + pageSize + "&page=" + page , {
      observe: 'response'
    });
  }

  addEntidad(entidad): Observable<EntidadDTO> {
    console.log(entidad);
    return this.http.post<HttpResponse<Object>>(dominio + 'entidad/saveorupdate', entidad).pipe(
      tap((entidad) => console.log(`added entidad w/ id=${entidad}`)),
      catchError(this.handleError<any>('addentidad'))
    );
  }

  updateEntidad(id, entidad): Observable<EntidadDTO> {
    return this.http.post<HttpResponse<Object>>(dominio + 'entidad/saveorupdate/' + id, entidad).pipe(
      tap(_ => console.log(`updated entidad id=${id}`)),
      catchError(this.handleError<any>('updateentidad'))
    );
  }

  deleteEntidad(id): Observable<EntidadDTO> {
    return this.http.post<HttpResponse<Object>>(dominio + 'entidad/delete/', id).pipe(
      tap(_ => console.log(`deleted entidad id=${id}`)),
      catchError(this.handleError<any>('deleteentidad'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  obtenerEntidadPorid(id: string): Observable<HttpResponse<EntidadDTO>> {
    console.log('Obtener entidadPorid');
    return this.http.get<HttpResponse<Object>>(dominio + "entidad/ById/" + id);
  }

  obtenerEntidadPornit(nit: string): Observable<HttpResponse<EntidadDTO>> {
    console.log('Obtener entidadPornit');
    return this.http.get<HttpResponse<Object>>(dominio + "entidad/ByNit/" + nit);
  }

  obtenerEntidadPornombre(nombre: string): Observable<HttpResponse<EntidadDTO>> {
    console.log('Obtener entidadPornombre');
    return this.http.get<HttpResponse<Object>>(dominio + "entidad/ByNombre/" + nombre);
  }




}

