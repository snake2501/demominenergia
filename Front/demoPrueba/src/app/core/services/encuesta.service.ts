import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Page } from '../../common/util';
import { EncuestaDTO} from '../../pages/encuesta/encuesta.dto';
import { dominio } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class encuestaService {

  constructor(private http: HttpClient) { }

   all(page, pageSize, nameLike, sortBy, sortOrder): Observable<Object> {
    console.log('all encuesta');
	let sort = "";
    if (sortBy !== undefined) {
      sort = "&sortBy=" + page ;
    }
	return this.http.get<HttpResponse<Object>>(dominio +  "encuesta/all?filtro=" + nameLike + "&size=" + pageSize + "&page=" + page + sort + "&sortOrderBy=" + sortOrder, {
    observe: 'response'
  });
  }  
  
  addEncuesta(encuesta): Observable<EncuestaDTO> {
    console.log(encuesta);
    return this.http.post<HttpResponse<Object>>(dominio +  'encuesta/saveorupdate', encuesta).pipe(
      tap((encuesta) => console.log(`added encuesta w/ id=${encuesta}`)),
      catchError(this.handleError<any>('addencuesta'))
    );
  }

  updateEncuesta(id, encuesta): Observable<EncuestaDTO> {
    return this.http.post<HttpResponse<Object>>(dominio +  'encuesta/saveorupdate/' + id, encuesta).pipe(
      tap(_ => console.log(`updated encuesta id=${id}`)),
      catchError(this.handleError<any>('updateencuesta'))
    );
  }

  deleteEncuesta(id): Observable<EncuestaDTO> {
    return this.http.post<HttpResponse<Object>>(dominio +  'encuesta/delete/' , id).pipe(
      tap(_ => console.log(`deleted encuesta id=${id}`)),
      catchError(this.handleError<any>('deleteencuesta'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  

  obtenerEncuestaPorid(id: string): Observable<HttpResponse<EncuestaDTO>> {
    console.log('Obtener encuestaPorid');
    return this.http.get<HttpResponse<Object>>(dominio +  "encuesta/ById/"+id);
  }

  obtenerEncuestaPorid_usuario(id_usuario: string): Observable<HttpResponse<EncuestaDTO>> {
    console.log('Obtener encuestaPorid_usuario');
    return this.http.get<HttpResponse<Object>>(dominio +  "encuesta/ById_usuario/"+id_usuario);
  }

  obtenerEncuestaPorndocumento(ndocumento: string): Observable<HttpResponse<EncuestaDTO>> {
    console.log('Obtener encuestaPorndocumento');
    return this.http.get<HttpResponse<Object>>(dominio +  "encuesta/ByNdocumento/"+ndocumento);
  }

  obtenerEncuestaPoremail(email: string): Observable<HttpResponse<EncuestaDTO>> {
    console.log('Obtener encuestaPoremail');
    return this.http.get<HttpResponse<Object>>(dominio +  "encuesta/ByEmail/"+email);
  }

  obtenerEncuestaPorcomentario(comentario: string): Observable<HttpResponse<EncuestaDTO>> {
    console.log('Obtener encuestaPorcomentario');
    return this.http.get<HttpResponse<Object>>(dominio +  "encuesta/ByComentario/"+comentario);
  }

  obtenerEncuestaPormarcafavorita(marcafavorita: string): Observable<HttpResponse<EncuestaDTO>> {
    console.log('Obtener encuestaPormarcafavorita');
    return this.http.get<HttpResponse<Object>>(dominio +  "encuesta/ByMarcafavorita/"+marcafavorita);
  }

  obtenerEncuestaPorfecha(fecha: string): Observable<HttpResponse<EncuestaDTO>> {
    console.log('Obtener encuestaPorfecha');
    return this.http.get<HttpResponse<Object>>(dominio +  "encuesta/ByFecha/"+fecha);
  }






}

