import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { UtilService } from './util.service';
import { Router } from '@angular/router';
import { Usuario } from '../model/usuario.model';
import { dominio } from 'src/environments/environment';
import { Menu } from '../model/menu.model';
import { Observable } from 'rxjs';
import { v4 as uuid } from 'uuid';

export const ROLID_MENU = [
  {
    rol: "Admin",
    menu: [
      {
        titulo: 'Entidad',
        descripcion: '',
        icono: 'fa fa-bath',
        url: '/EntidadList'
      },
      {
        titulo: 'Usuarios',
        descripcion: '',
        icono: 'fa fa-bath',
        url: '/UsuarioList'
      },
      {
        titulo: 'Contenido',
        descripcion: '',
        icono: 'fa fa-bath',
        url: '/ContenidoList'
      },
    ]
  },
];

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  public _usuarioConectado: Usuario;
  public _token: string;
  private _menu: Array<Menu>;
  public _encuestaCurso: number;

  constructor(private router: Router, private http: HttpClient, private _util: UtilService) { }

  cerrarSesion() {
    this._usuarioConectado = null;
    sessionStorage.clear();
  }


  iniciarSesion(credenciales: object): Observable<any> {
    console.log(uuid());
    console.log(credenciales);
    credenciales['uuid'] = uuid();
    console.log(JSON.stringify(credenciales));
    return this.http.post(dominio + "Autenticacion/login", JSON.stringify(credenciales), {});
  }


  inicializarSesionUSuario(usuConectado: Usuario) {
    sessionStorage.setItem("tokenApp", "");
    sessionStorage.setItem("usuarioConectadoApp", JSON.stringify(usuConectado));
  }


  private obtenerRolMenu(r: string): Array<Menu> {
    for (let item of ROLID_MENU) {
      return item.menu;
    }
  }

  get token(): string {
    this._token = sessionStorage.getItem("tokenApp");
    return this._token;
  }

  set token(tid: string) {
    sessionStorage.setItem("tokenApp", tid);
    this._token = tid;
  }

  get usuarioConectado(): Usuario {
    this._usuarioConectado = JSON.parse(sessionStorage.getItem("usuarioConectadoApp"));
    return this._usuarioConectado;
  }

  set usuarioConectado(usu: Usuario) {
    sessionStorage.setItem("usuarioConectadoApp", JSON.stringify(usu));
    this._usuarioConectado = usu;
  }

  get autenticado(): boolean {
    return sessionStorage.getItem("usuarioConectadoApp") ? true : false;
  }

  get menu(): Array<Menu> {
    if (this.autenticado)
      this._menu = this.obtenerRolMenu("");
    return this._menu;
  }

  get encuestaCurso(): number {
    this._encuestaCurso = +sessionStorage.getItem("encuestaCurso");
    return this._encuestaCurso;
  }

  setEncuestaCurso(eec: number) {
    sessionStorage.setItem("encuestaCurso", "" + eec);
    this._encuestaCurso = eec;
  }


}


