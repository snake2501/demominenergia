
export class Page<T> {
    content: T[] = [];
    totalPages: number = 0;
    totalElements: number = 0;
    last: boolean = false;
    size: number = 0;
    first: boolean = true;
    sort: string = null;
    numberOfElements: number = 0;
}

export class Pageable {
    pageSize: number = 10;
    page: number = 0;
}


