import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { MenubarModule } from 'primeng/menubar';
import { MenuModule } from 'primeng/menu';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextModule } from 'primeng/inputtext';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { EncabezadoComponent } from './layout/encabezado/encabezado.component';
import { MenuComponent } from './layout/menu/menu.component';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { LoginComponent } from './pages/login/login.component';
import { InterceptorService } from './core/services/interceptor.service';
import { TabViewModule } from 'primeng/tabview';
import { MessageService } from 'primeng/api';
import { DialogModule } from 'primeng/dialog';
import {CalendarModule} from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { PanelModule } from 'primeng/panel';
import { TooltipModule } from 'primeng/tooltip';
import { AccordionModule } from 'primeng/accordion';

import { UsuarioListComponent } from './pages/usuario/usuario-list.component';
import { ContenidoListComponent } from './pages/contenido/contenido-list.component';
import { EntidadListComponent } from './pages/entidad/entidad-list.component';
import { AutenticacionGuardService} from './core/services/autenticacion-guard.service'

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent, data: { title: 'LoginComponent' } },
  { path: 'UsuarioList', component: UsuarioListComponent,canActivate: [AutenticacionGuardService], data: { title: 'usuarioList' } },
  { path: 'ContenidoList', component: ContenidoListComponent,canActivate: [AutenticacionGuardService], data: { title: 'contenidoList' } },
  { path: 'EntidadList', component: EntidadListComponent,canActivate: [AutenticacionGuardService], data: { title: 'entidadList' } },
  { path: "**", pathMatch: "full", redirectTo: "login" }
];

@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    MenuComponent,
    LoginComponent,
    SpinnerComponent,
    UsuarioListComponent,
    ContenidoListComponent,
    EntidadListComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    BrowserModule,
    ButtonModule,
    DialogModule,
    CardModule,
    DropdownModule,
    FormsModule,
    HttpClientModule,
    InputTextModule,
    MenubarModule,
    MenuModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OverlayPanelModule,
    PanelModule,
    CalendarModule,
    ReactiveFormsModule,
    TableModule,
    TabViewModule,
    ToastModule,
    TooltipModule,
    AccordionModule,
  ],
  providers: [
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



